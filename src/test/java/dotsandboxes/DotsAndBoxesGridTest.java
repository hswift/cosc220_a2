package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }


    /*
     * This tests the boxComplete() function.
     */
    @Test
    public void testForBoxComplete() {
        logger.info("Testing if box is complete.");

        // Create new grid instance
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(3, 3, 2);  

        // Draw half a box
        grid.drawVertical(1, 1, 1);
        grid.drawVertical(2, 1, 1);

        // Test boxComplete() function
        assertFalse(grid.boxComplete(1, 1));

        // Draw remaining lines
        grid.drawHorizontal(1, 1, 1);
        grid.drawHorizontal(1, 2, 1);

        // Test boxComplete() function
        assertTrue(grid.boxComplete(1, 1));
    }

    /*
     * Test for drawHorizontal() function.
     */
    @Test
    public void testDrawHorizontal() {
        logger.info("Testing drawHorizontal()");

        // Initialise new DotsAndBoxesGrid instance and new player
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(3, 3, 2);
        int player = 1;

        // Draw new line at (1, 1). This should return false as the box is not complete.
        assertFalse(grid.drawHorizontal(1, 1, player));

        // Try redraw line at (1, 1). This should throw an IllegalStateException.
        assertThrows(IllegalStateException.class, () -> {
            grid.drawHorizontal(1, 1, player);
        });
    }

    /*
     * Test for drawVertical() function.
     */
    @Test
    public void testDrawVertical() {
        logger.info("Testing drawVertical()");

        // Initialise new DotsAndBoxesGrid instance and new player
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(3, 3, 2);
        int player = 1;

        // Draw new line at (1, 1). This should return false as the box is not complete.
        assertFalse(grid.drawVertical(1, 1, player));

        // Try redraw line at (1, 1). This should throw an IllegalStateException.
        assertThrows(IllegalStateException.class, () -> {
            grid.drawVertical(1, 1, player);
        });
    }
}
